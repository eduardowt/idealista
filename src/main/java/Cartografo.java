public interface Cartografo {

    int obtenerIdentificadorUrbanizacion(double coordenadaX, double coordenadaY);

    int obtenerAdyacente(int identificadorUrbanizacionOrigen, Direccion direccion);
}
