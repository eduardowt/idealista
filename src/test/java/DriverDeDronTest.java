import org.junit.Test;

import java.util.Set;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyDouble;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class DriverDeDronTest {

    private Cartografo cartografo = mock(Cartografo.class);
    private DriverDeDron driver = new DriverDeDron(cartografo);

    @Test
    public void la_urbe_donde_se_situa_el_dron_forma_parte_de_la_solucion() throws Exception {

        when(cartografo.obtenerIdentificadorUrbanizacion(anyDouble(), anyDouble())).thenReturn(1);
        when(cartografo.obtenerAdyacente(1, Direccion.ABAJO)).thenReturn(8);

        Set<Integer> urbesAVisitar = driver.obtenerUrbanizaciones(3.2, 4.5, 3);

        assertTrue(urbesAVisitar.contains(1));
    }

    @Test(expected = IllegalArgumentException.class)
    public void pedir_las_urbanizaciones_de_un_rango_inexistente_es_un_error() throws Exception {

        when(cartografo.obtenerIdentificadorUrbanizacion(anyDouble(), anyDouble())).thenReturn(1);
        when(cartografo.obtenerAdyacente(1, Direccion.ABAJO)).thenReturn(6);

        driver.obtenerUrbanizaciones(3.2, 4.5, 3);
    }

    @Test
    public void caso_basico_solo_hay_una_urbanizacion() throws Exception {

        when(cartografo.obtenerIdentificadorUrbanizacion(anyDouble(), anyDouble())).thenReturn(1);
        when(cartografo.obtenerAdyacente(1, Direccion.ABAJO)).thenReturn(-1);
        when(cartografo.obtenerAdyacente(1, Direccion.ARRIBA)).thenReturn(-1);
        when(cartografo.obtenerAdyacente(1, Direccion.IZQUIERDA)).thenReturn(-1);
        when(cartografo.obtenerAdyacente(1, Direccion.DERECHA)).thenReturn(-1);

        Set<Integer> urbesAVisitar = driver.obtenerUrbanizaciones(3.2, 4.5, 0);

        assertThat(urbesAVisitar.size(), is(1));
        assertTrue(urbesAVisitar.contains(1));
    }

    @Test
    public void deberia_incluir_todas_las_urbanizaciones_a_visitar() throws Exception {

        when(cartografo.obtenerIdentificadorUrbanizacion(anyDouble(), anyDouble())).thenReturn(49);
        when(cartografo.obtenerAdyacente(49, Direccion.ABAJO)).thenReturn(-1);
        when(cartografo.obtenerAdyacente(49, Direccion.ARRIBA)).thenReturn(42);

        Set<Integer> urbesAVisitar = driver.obtenerUrbanizaciones(3.2, 4.5, 1);

        assertThat(urbesAVisitar.size(), is(9));
        assertTrue(urbesAVisitar.contains(49));
        assertTrue(urbesAVisitar.contains(17));
        assertTrue(urbesAVisitar.contains(18));
        assertTrue(urbesAVisitar.contains(19));
        assertTrue(urbesAVisitar.contains(24));
        assertTrue(urbesAVisitar.contains(26));
        assertTrue(urbesAVisitar.contains(31));
        assertTrue(urbesAVisitar.contains(32));
        assertTrue(urbesAVisitar.contains(33));
    }
}