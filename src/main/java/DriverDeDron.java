import java.util.HashSet;
import java.util.Set;

//https://bitbucket.org/eduardowt/idealista
public class DriverDeDron {

    private static final int FUERA_DE_LA_PARCELA = -1;
    private final Cartografo cartografo;

    public DriverDeDron(Cartografo cartografo) {
        this.cartografo = cartografo;
    }

    public Set<Integer> obtenerUrbanizaciones(double coordenadaX, double coordenadaY, int rango) {
        HashSet<Integer> urbesAVisitar = new HashSet<>();

        int urbeDelDron = cartografo.obtenerIdentificadorUrbanizacion(coordenadaX, coordenadaY);
        urbesAVisitar.add(urbeDelDron);

        int longitudDeLaParcela = longitudDeParcela(urbeDelDron);
        int rangoMaximoDeLaParcela = maximoRangoDeParcelaDeLongitud(longitudDeLaParcela);

        if (rango > rangoMaximoDeLaParcela) {
            throw new IllegalArgumentException("La parcela tiene un rango maximo de " + rangoMaximoDeLaParcela
                    + " pero ha pedido las urbanizaciones de un rango mayor (" + rango + ")");
        }

        int menorUrbanizacionDeRango = menorUrbanizacionDeRango(rango, rangoMaximoDeLaParcela, longitudDeLaParcela);
        int longitudDeRango = longitudDeRango(rango);

        //el set se encarga de que no visitemos ninguna urbanizacion dos veces
        urbesAVisitar.addAll(urbanizacionesPorArriba(menorUrbanizacionDeRango, longitudDeRango));
        urbesAVisitar.addAll(urbanizacionesPorAbajo(menorUrbanizacionDeRango, longitudDeRango, longitudDeLaParcela));
        urbesAVisitar.addAll(urbanizacionesPorLaIzquierda(menorUrbanizacionDeRango, longitudDeRango, longitudDeLaParcela));
        urbesAVisitar.addAll(urbanizacionesPorLaDerecha(menorUrbanizacionDeRango, longitudDeRango, longitudDeLaParcela));

        return urbesAVisitar;
    }

    private int longitudDeRango(int rango) {
        return 2 * rango + 1;
    }

    private Set<Integer> urbanizacionesPorLaDerecha(int menorUrbanizacionDeRango, int longitudDeRango,
                                                    int longitudDeLaParcela) {
        HashSet<Integer> urbanizacionesPorLaDerecha = new HashSet<>();
        int esquinaArribaDerechaDelRango = menorUrbanizacionDeRango + longitudDeRango - 1;

        for (int i = 0; i < longitudDeRango; i++) {
            urbanizacionesPorLaDerecha.add(esquinaArribaDerechaDelRango + i * longitudDeLaParcela);
        }

        return urbanizacionesPorLaDerecha;
    }

    private Set<Integer> urbanizacionesPorLaIzquierda(int esquinaArribaIzquierdaDelRango, int longitudDeRango,
                                                      int longitudDeLaParcela) {
        HashSet<Integer> urbanizacionesPorLaIzquierda = new HashSet<>();

        for (int i = 0; i < longitudDeRango; i++) {
            urbanizacionesPorLaIzquierda.add(esquinaArribaIzquierdaDelRango + i * longitudDeLaParcela);
        }
        return urbanizacionesPorLaIzquierda;
    }

    private Set<Integer> urbanizacionesPorAbajo(int menorUrbanizacionDeRango, int longitudDeRango,
                                                int longitudDeLaParcela) {
        HashSet<Integer> urbanizacionesPorAbajo = new HashSet<>();
        int esquinaAbajoIzquierdaDelRango = menorUrbanizacionDeRango + longitudDeLaParcela * (longitudDeRango - 1);

        for (int i = 0; i < longitudDeRango; i++) {
            urbanizacionesPorAbajo.add(esquinaAbajoIzquierdaDelRango + i);
        }
        return urbanizacionesPorAbajo;
    }

    private Set<Integer> urbanizacionesPorArriba(int esquinaArribaIzquierdaDelRango, int longitudDeRango) {
        HashSet<Integer> urbanizacionesPorArriba = new HashSet<>();
        for (int i = 0; i < longitudDeRango; i++) {
            urbanizacionesPorArriba.add(esquinaArribaIzquierdaDelRango + i);
        }
        return urbanizacionesPorArriba;
    }

    private int menorUrbanizacionDeRango(int rango, int rangoMaximoDeLaParcela, int longitudDeLaParcela) {
        return (rangoMaximoDeLaParcela - rango) * (longitudDeLaParcela + 1) + 1;
    }

    //si una urbanizacion no tiene adyacente por una direccion, mira al contrario para calcular el tamano de la parcela
    //si no tiene urbanizaciones adyacentes a direcciones opuestas es que solo hay una urbanizacion
    private int longitudDeParcela(int urbeDelDron) {
        int urbeDebajoDelDron = cartografo.obtenerAdyacente(urbeDelDron, Direccion.ABAJO);

        if (urbeDebajoDelDron == FUERA_DE_LA_PARCELA) {
            int urbeArribaDelDron = cartografo.obtenerAdyacente(urbeDelDron, Direccion.ARRIBA);

            return urbeArribaDelDron == FUERA_DE_LA_PARCELA ? 1 : urbeDelDron - urbeArribaDelDron;

        } else {
            return urbeDebajoDelDron - urbeDelDron;
        }
    }

    //medida en urbanizaciones
    private int maximoRangoDeParcelaDeLongitud(int longitudDeLaParcela) {
        return (longitudDeLaParcela - 1) / 2;
    }
}
